"use strict"

const dom = require('../dom.js')
const utils = require('../utils')

class AttachmentViewer {
    constructor(project) {
        this._project = project
    }

    showAttachment(attachment) {
        if(window.$images) {
            // use the native plugin to show the image
            this._project.getAsset(attachment, (err, blob) => {
                utils.dataUrlFromBlob(blob, window.$images.showImage)
            })
        } else {
            var assetInfo = this._project.getAssetInfo(attachment)

            var img = dom.element("img", undefined, "fullwidth")
            img.src = ""
            this._project.getAssetUrl(attachment, url => {
                img.src = url
            })

            var modal = dom.modal(assetInfo.name, true)
            modal.appendChild(img)
            modal.show()
        }
    }
}

module.exports = AttachmentViewer
